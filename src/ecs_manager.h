#include <map>
#include <set>
#include <vector>
#include <string>
#include <mutex>
#include "entity.h"
#include "component.h"

// Map of Component Data keyed by Entity Id
typedef std::map<EntityId, Component> ComponentMap;

class EcsManager {
public:
  // Remove copy constructor
  EcsManager(EcsManager& ecsManager) = delete;

  // Remove assignable
  void operator=(const EcsManager&) = delete;

  // Get an instance of the EcsManager
  static EcsManager* getInstance();

  // Creates an Entity
  // Returns the new Entity Id
  EntityId createEntity();

  // Returns a set of Entity Ids
  std::set<EntityId> getEntityList() { return m_entityList; }

  // Returns a vector of Components associated with the Entity
  std::vector<Component> getEntity(EntityId eid);

  // Deletes an Entity and all associated Components
  void deleteEntity(EntityId eid);

  // Adds an Entity to the Component List
  // Existing Component is overwritten
  // If Component doesn't exist it is created
  // Returns False if the EntityId does not exist
  bool setComponent(EntityId eid, Component component);

  // Removes the Component data associated with the Entity Id
  void removeComponent(EntityId eid, std::string componentName);

  // Returns a list
  // Returns the entire List of Components
  ComponentMap getComponentList(std::string name);

  // Returns the Component associated with the Entity
  Component getComponent(EntityId eid, std::string name);

  // Returns a list of all available Component Names
  std::set<std::string> getComponentNames();

  // Sets an entire list of component data
  bool setComponentList (ComponentMap componentMap);

protected:
  // The instance of this class
  static EcsManager* m_instance;

  // Default Constructor
  EcsManager();
  // Default Destructor
  ~EcsManager();

private:

  // Increments for each entity created, used to generate UniqueIds for each 
  // new entity
  static EntityId s_entityCounter;

  // Mutex Lock for Singleton
  static std::mutex s_mutexLock;

  // Vector containing valid Entity Ids
  std::set<EntityId> m_entityList;
  // Map of Component Maps
  std::map<std::string, ComponentMap> m_componentPool;
  // Map to quickly lookup Components associated with an Entity
  std::map<EntityId, std::set<std::string>> m_entityComponentMap;

  // Checks if the EntityId exists
  bool entityIsValid (EntityId eid);

  // Checks if the Component exists
  bool componentIsValid (std::string name);
};
// TODO: Remove Sets and Maps, they cause too many allocations and potential
//       cache misses. This is counter to Data Oriented Design and (ironically)
//       by inheritance, ECS.
// TODO: Add hooks for periodic component / entity updates
// TODO: Add hooks for issueing interrupts to subscribed systems
// TODO: Add hooks for remote synchronization
// TODO: Add hooks for persistant backup to a SQL DB (Might be better as its
//       own system or a wrapper around this
// TODO: Wrap in clean RESTful API
// TODO: Documentation
// TODO: Make Docker Container
// TODO: Create System framework (independent docker apps)
