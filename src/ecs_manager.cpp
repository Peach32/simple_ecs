#include "ecs_manager.h"
#include <algorithm>

// Instantiate static member variables
EntityId EcsManager::s_entityCounter = 0;
EcsManager* EcsManager::m_instance = nullptr;
std::mutex EcsManager::s_mutexLock;

// Get an instance of the EcsManager
EcsManager* EcsManager::getInstance() {
  // Safely attempt to lock the mutex, released on destruction
  std::lock_guard<std::mutex> lock (s_mutexLock);
  if (m_instance == nullptr) {
    m_instance = new EcsManager();
  }
  return m_instance;
}

EcsManager::EcsManager () {}

EcsManager::~EcsManager () {
  m_entityList.clear();
  m_componentPool.clear();
  m_entityComponentMap.clear();
  s_entityCounter = 0;
  delete m_instance;
}

// Creates an Entity
// Returns the new Entity Id
EntityId EcsManager::createEntity () {
  EntityId eid = s_entityCounter++;
  m_entityList.insert (eid);
  return eid;
}

// Returns a vector of Components associated with the Entity
std::vector<Component> EcsManager::getEntity (EntityId eid) {
  // Vector to be returned
  std::vector<Component> retVal = {};

  // Check if EntityId exists
  if (entityIsValid (eid)) {
    if (m_entityComponentMap.find (eid) != m_entityComponentMap.end()) {
      // Loop over all Component Names associated with an Entity
      for (std::string name : m_entityComponentMap.at (eid)) {
        // Check if the component exists
        if (componentIsValid (name)) {
          // Add the Component associated with the Entity to the return vector
          retVal.push_back (m_componentPool.at (name).at (eid));
        }
      }
    }
  }
  return retVal;
}

// Deletes an Entity and all associated Components
void EcsManager::deleteEntity (EntityId eid) {
  // Check if the EntityId exists
  if (entityIsValid (eid)) {
    // Check if EntityId has associated Components
    if (m_entityComponentMap.find (eid) != m_entityComponentMap.end()) {
      // Loop over all Component Names associated with an Entity
      std::set<std::string> componentNameList = m_entityComponentMap.at (eid);
      for (std::string componentName : componentNameList) {
        // Remove that Component data
        removeComponent(eid, componentName);
      }
    }
    // Remove the EntityId from the EntityId list
     m_entityList.erase (eid);
  }
}

// Adds an Entity to the Component List
// Existing Component is overwritten
// If Component doesn't exist it is created
// Returns False if the EntityId does not exist
bool EcsManager::setComponent (EntityId eid, Component component) {
  // Return Value, assume EntityId does not exist
  bool retVal = false;

  // Check if the EntityId exists
  if (entityIsValid (eid)) {
    // Save off the Component Name
    std::string componentName = component.m_header.m_name;

    // If the Component exists, update it
    if (componentIsValid (componentName)) {
      // Check if EntityId exists in ComponentMap
      if (m_componentPool.at (componentName).find (eid) !=
        m_componentPool.at (componentName).end ()) {
        // Remove the existing Component
        m_componentPool.at (componentName).erase (eid);
      } else { // EntityId does not exist in the ComponentMap
        // Add the Entity to the EntityComponentMap
        // Create temperary set
        std::set<std::string> temp;
        // Add the ComponentName to the set
        temp.insert (componentName);
        // Insert the EntityId and set of ComponentNames to the EntityComponentMap
        m_entityComponentMap.insert (std::make_pair (eid, temp));
      }
      // Add the new Component
      m_componentPool.at (componentName).insert (std::make_pair (eid, component));
    } else { // Component does not exist, create it
      // Create new ComponentMap
      ComponentMap componentMap;
      // Add Entity and Component to ComponentMap
      componentMap.insert (std::make_pair (eid, component));
      // Add ComponentMap to ComponentPool
      m_componentPool.insert (std::make_pair (componentName, componentMap));
      if (m_entityComponentMap.find (eid) == m_entityComponentMap.end()) {
        std::set<std::string> temp;
        temp.insert (componentName);
        // Insert Entity and ComponentName to entityComponentMap
        m_entityComponentMap.insert (std::make_pair (eid, temp));
      } else {
        // Add Entity and ComponentName to entityComponentMap
        m_entityComponentMap.at (eid).insert (componentName);
      }
    }
    // Entity exists, component was set
    retVal = true;
  }
  return retVal;
}

// Removes the Component data associated with the Entity Id
void EcsManager::removeComponent (EntityId eid, std::string componentName) {
  // Check if EntityId is Valid
  if (entityIsValid (eid)) {
    // Check if Component is Valid
    if (componentIsValid (componentName)) {
      // Check if the EntityId is associated with the Component
      if (m_componentPool.at (componentName).find (eid) !=
            m_componentPool.at (componentName).end()) {
        // Remove Component from ComponentMap
        m_componentPool.at (componentName).erase (eid);
      }
      // Check if Component is associated with the EntityId
      if (m_entityComponentMap.at (eid).find (componentName) !=
            m_entityComponentMap.at (eid).end()) {
        // Remove Component from EntityId in entityComponentMap
        m_entityComponentMap.at (eid).erase (componentName);
      }
    }
  }
}

// Returns the entire List of Components
ComponentMap EcsManager::getComponentList(std::string name) {
  ComponentMap retVal = {};
  if (m_componentPool.find (name) != m_componentPool.end()) {
    retVal = m_componentPool.at (name);
  }
  return retVal;
}

// Returns the Component associated with the Entity
Component EcsManager::getComponent(EntityId eid, std::string name) {
  Component retVal = emptyComponent;

  // Check if the EntityId exists
  if (entityIsValid (eid)) {
    // Check if the Component exists
    if (componentIsValid (name)) {
      if (m_componentPool.at (name).find (eid) !=
            m_componentPool.at (name).end()) {
        retVal = m_componentPool.at (name).at (eid);
      }
    }
  }
  return retVal;
}

// Returns a list of all available Component Names
std::set<std::string> EcsManager::getComponentNames() {
  // Value to be returned
  std::set<std::string> retVal;

  // Loop over all existing components in the component pool
  for (std::pair<std::string, ComponentMap> componentPair : m_componentPool) {
    // Extract the component names and save them off
    retVal.insert (componentPair.first);
  }

  // Return the list of component names
  return retVal;
}

// Sets an entire list of component data
bool EcsManager::setComponentList (ComponentMap componentMap) {
  // Set the return value, assume the worst
  bool retVal = false;

  // Loop over the component list
  for (std::pair<EntityId, Component> componentPair : componentMap) {
    // Set the Component
    retVal = retVal && setComponent (componentPair.first, componentPair.second);
  }

  // return the status of adding the list of components
  return retVal;
}

/////////////
// Private //
/////////////

// Checks the list of Entities for the given EntityId
bool EcsManager::entityIsValid (EntityId eid) {
  return find (m_entityList.begin(), m_entityList.end(), eid) !=
    m_entityList.end();
}

// Checks if the Component exists in the ComponentPool
bool EcsManager::componentIsValid (std::string name) {
  return m_componentPool.find (name) != m_componentPool.end();
}

