#include <string>

struct Header {
  size_t m_sizeBytes;
  std::string m_name;
};

struct Component {
  Header m_header;
  uint8_t* m_data;
};

const Component emptyComponent = { {0, ""}, 0};
