#include <iostream>
#include "ecs_manager.h"

void printEntityList () {
  EcsManager* ecsManager = EcsManager::getInstance();
  for (EntityId eid : ecsManager->getEntityList()) {
    std::cout << eid << std::endl;
  }
}

int main(int, char**) {
  std::cout << std::endl << "Hello, World" << std::endl;

  EcsManager* ecsManager = EcsManager::getInstance();
  Component component;

  std::cout << std::endl << "Create Entities" << std::endl;
  // Create 10 Entities
  for (int i = 0; i < 10; i++) {
    ecsManager->createEntity();
  }

  // Print all Entity Ids
  printEntityList();

  std::cout << std::endl << "Delete Entities" << std::endl;

  // Delete a few of them
  for (int i = 3; i < 7; i++) {
    ecsManager->deleteEntity(i);
  }

  // Print all Entity Ids
  printEntityList();

  std::cout << std::endl << "Set Component" << std::endl;

  uint32_t testInt = 10;
  component.m_header.m_name = "Test";
  component.m_data = reinterpret_cast<uint8_t*> (&testInt);
  component.m_header.m_sizeBytes = sizeof(testInt);

  bool retVal = ecsManager->setComponent (1, component);
  if (retVal) {
    std::cout << "Success" << std::endl;
  } else {
    std::cout << "Failure" << std::endl;
  }

  retVal = ecsManager->setComponent (4, component);
  if (retVal) {
    std::cout << "Success" << std::endl;
  } else {
    std::cout << "Failure" << std::endl;
  }

  std::cout << std::endl << "Get Entity" << std::endl;
  for (EntityId eid : ecsManager->getEntityList()) {
    std::cout << "EntityId: " << eid << std::endl;
    if (ecsManager->getEntity (eid).size() > 0) {
      for (Component comp : ecsManager->getEntity (eid)) {
        std::cout << "Component: " << std::endl;
        std::cout << "  Name : " << comp.m_header.m_name << std::endl;
        uint32_t value = *(reinterpret_cast<uint32_t*> (comp.m_data));
        std::cout << "  Value: " << value << std::endl;
      }
    } else {
      std::cout << "Component: NULL" << std::endl;
    }
  }

  std::cout << std::endl << "Set Component Again" << std::endl;
  testInt = 20;

  retVal = ecsManager->setComponent (1, component);
  if (retVal) {
    std::cout << "Success" << std::endl;
  } else {
    std::cout << "Failure" << std::endl;
  }

  // Clear the contents of the component
  component = emptyComponent;

  std::cout << std::endl << "Get Entity Again" << std::endl;
  for (EntityId eid = 0; eid < 10; eid++) {
    std::cout << "EntityId: " << eid << std::endl;
    if (ecsManager->getEntity (eid).size() > 0) {
      for (Component comp : ecsManager->getEntity (eid)) {
        std::cout << "Component: " << std::endl;
        std::cout << "  Name : " << comp.m_header.m_name << std::endl;
        uint32_t value = *(reinterpret_cast<uint32_t*> (comp.m_data));
        std::cout << "  Value: " << value << std::endl;
      }
    } else {
      std::cout << "Component: NULL" << std::endl;
    }
  }

  std::cout << std::endl << "Get Component" << std::endl;
  for (std::pair<EntityId, Component> componentPair : ecsManager->getComponentList("Test")) {
    std::cout << std::endl << "EntityId : " << componentPair.first << std::endl;
    std::cout << "Component: " << componentPair.second.m_header.m_name << std::endl;
  }
  for (std::pair<EntityId, Component> componentPair : ecsManager->getComponentList("test")) {
    std::cout << std::endl << "EntityId : " << componentPair.first << std::endl;
    std::cout << "Component: " << componentPair.second.m_header.m_name << std::endl;
  }

  std::cout << std::endl << "Get Entity Component Pair" << std::endl;
  for (EntityId eid : ecsManager->getEntityList()) {
    component = ecsManager->getComponent (eid, "Test");
    std::cout << std::endl;
    std::cout << "EntityId : " << eid << std::endl;
    std::cout << "Component: " << std::endl;
    if (component.m_header.m_sizeBytes > 0) {
      std::cout << "  Name : " << component.m_header.m_name << std::endl;
      uint32_t value = *(reinterpret_cast<uint32_t*> (component.m_data));
      std::cout << "  Value: " << value << std::endl;
    }

    component = ecsManager->getComponent (eid, "test");
    std::cout << std::endl;
    std::cout << "EntityId : " << eid << std::endl;
    std::cout << "Component: " << component.m_header.m_name << std::endl;
  }

  std::cout << std::endl << "Remove Component" << std::endl;
  testInt = 30;
  component.m_header.m_name = "test";
  component.m_data = reinterpret_cast<uint8_t*> (&testInt);
  component.m_header.m_sizeBytes = sizeof(testInt);

  retVal = ecsManager->setComponent (1, component);
  if (retVal) {
    std::cout << "Success" << std::endl;
  } else {
    std::cout << "Failure" << std::endl;
  }

  for (Component comp : ecsManager->getEntity (1)) {
    std::cout << "Component: " << std::endl;
    std::cout << "  Name : " << comp.m_header.m_name << std::endl;
    uint32_t value = *(reinterpret_cast<uint32_t*> (comp.m_data));
    std::cout << "  Value: " << value << std::endl;
  }

  std::cout << "  Remove valid EntityId and Component" << std::endl;
  ecsManager->removeComponent (1, "Test");

  for (Component comp : ecsManager->getEntity (1)) {
    std::cout << "Component: " << std::endl;
    std::cout << "  Name : " << comp.m_header.m_name << std::endl;
    uint32_t value = *(reinterpret_cast<uint32_t*> (comp.m_data));
    std::cout << "  Value: " << value << std::endl;
  }

  std::cout << "  Remove invalid EntityId" << std::endl;
  ecsManager->removeComponent (3, "Test");
  std::cout << "  Remove invalid Component" << std::endl;
  ecsManager->removeComponent (1, "Test");

  std::cout << std::endl << "Delete All Entities" << std::endl;
  for (EntityId eid : ecsManager->getEntityList()) {
    std::cout << "  Removing EntityId: " << eid << std::endl;
    ecsManager->deleteEntity (eid);
  }

  for (EntityId eid : ecsManager->getEntityList()) {
    std::cout << "EntityId: " << eid << std::endl;
    if (ecsManager->getEntity (eid).size() > 0) {
      for (Component comp : ecsManager->getEntity (eid)) {
        std::cout << "Component: " << std::endl;
        std::cout << "  Name : " << comp.m_header.m_name << std::endl;
        uint32_t value = *(reinterpret_cast<uint32_t*> (comp.m_data));
        std::cout << "  Value: " << value << std::endl;
      }
    } else {
      std::cout << "Component: NULL" << std::endl;
    }
  }

  return 0;
}

